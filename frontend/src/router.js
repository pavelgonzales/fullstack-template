import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Handbook from './views/Handbook.vue'
import Profile from './views/Profile.vue';
import Achievment from './views/Achievment.vue'
import Add from './views/Add.vue';
import Chat from './views/Chat.vue';
import Auth from './views/Auth.vue';

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Авторизация',
      component: Auth
    },
    {
      path: '/stats',
      name: 'Мое здоровье',
      component: Home
    },
    {
      path: '/handbook',
      name: 'Полезно знать',
      component: Handbook
    },
    {
      path: '/profile',
      name: 'profile',
      component: Profile
    },
    {
      path: '/achievment',
      name: 'Бонусы и награды',
      component: Achievment
    },
    {
      path: '/add',
      name: 'Новое устройство',
      component: Add
    },
    {
      path: '/chat',
      name: 'On-line консультация',
      component: Chat
    },
  ]
})
