import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import '../assets/styles/main.styl'

Vue.use(Vuetify, {
  options: {
    customProperties: true
  },
  iconfont: 'md',
})
