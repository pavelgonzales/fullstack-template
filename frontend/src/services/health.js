class Health {
  isCordova = false;
  constructor() {
    if (window.cordova) {
      this.isCordova = true;
    }
  }

  queryAggregated({ startDate, endDate, dataType, bucket }) {
    if (!this.isCordova) {
      return [];
    }

    return new Promise((resolve, reject) => {
      navigator.health.queryAggregated({
        startDate,
        endDate,
        dataType,
        bucket,
      }, resolve, reject);
    });
  }
}

export default new Health;


