import Vue from 'vue'
import Vuex from 'vuex'
import Health from './services/health'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    myInfo: null,
    isLoggedIn: false,
    isEmptyState: true
  },
  mutations: {
    setLoggedIn (state, value) {
      state.isLoggedIn = value
    },
    setMyInfo (state, data) {
      state.myInfo = data
    },
    setState (state, data) {
      state.isEmptyState = data
    }
  },
  actions: {
    SET_LOGIN ({ commit }, value) {
      commit('setLoggedIn', value)
    },
    SET_STATE ({ commit }, value) {
      commit('setState', value)
    },
    async getMyInfo ({ commit }, period) {
      if (!Health.isCordova) {
        commit('setMyInfo', [
          {
            unit: 'count',
            value: 9813,
            default: 7000
          },
          {
            unit: 'm',
            value: '6996.4',
            default: 15
          }
        ])

        return;
      }

      const types = ['steps', 'distance']
      const bucket = period
      let day

      if (period === 'day') {
        day = 1
      } else if (period === 'week') {
        day = 7
      } else if (period === 'mounth') {
        day = 30
      } else {
        day = 1
      }

      const startDate = new Date(new Date().getTime() - day * 24 * 60 * 60 * 1000)

      const promises = types.map((dataType) => {
        return Health.queryAggregated({
          startDate,
          endDate: new Date(),
          dataType,
          bucket
        })
      })

      let data

      try {
        data = await Promise.all(promises)
      } catch (error) {
        console.error(error)
        return;
      }

      const infoData = data.map((item) => {
        if (item.unit === 'm') {
          return {
            ...item,
            value: item.value.toFixed(1),
            'default': 7
          }
        } else if (item.unit === 'count') {
          return {
            ...item,
            'default': 8000
          }
        }
      })

      commit('setMyInfo', infoData)
    }
  }
})
